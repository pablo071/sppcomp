<?php

namespace WS\Components;

use Bitrix\Iblock\SectionTable;
use Bitrix\Main\Loader;
use WS\Entities\Question;
use WS\Tools\Module;
use WS\Tools\ORM\Db\Manager;

class TestData extends \CBitrixComponent {

    private $app;
    private $data;

    public function executeComponent() {

        Loader::includeModule('iblock');

        $this->getApp();
        $this->getData();

        $this->app->RestartBuffer();
        echo json_encode($this->data);
        die();
    }

    /**
     * @return Manager
     */
    private function ormManager() {
        return Module::getInstance()->getService('ormManager');
    }

    private function getData() {
        $sections = $this->getSections();
        $questions = $this->getQuestions();
        $item = [];

        /** @var Question $question */
        foreach ($questions as $question) {
            $item['questionId'] = $question->id;
            $item['question'] = $question->name;

            $url = '';
            if ($question->expert->logo->id) {
                $url = $question->expert->logo->getSrc();
            }

            $item['comment']['url'] = $url;
            $item['comment']['fullname'] = $question->expert->name;
            $item['comment']['post'] = ($question->expert->position) ? $question->expert->position : '';
            $item['comment']['workspace'] = ($question->expert->company) ? $question->expert->company : '';
            $item['comment']['text'] = $question->infoExpert;
            $item['options'] = $this->getAnswers($question);
            $sections[$question->section]['questions'][] = $item;
        }

        foreach ($sections as $section) {
            if (!isset($section['questions'])) {
                continue;
            }
            $count = 0;
            foreach ($section['questions'] as & $question) {
                $count++;
                $question['id'] = $count;
            }
            $this->data[] = $section;
        }
    }

    private function getSections() {
        $sections = [];
        $res = SectionTable::getList(
            array(
                "select" => ["ID", "NAME"],
                "filter" => ["IBLOCK_ID" => IBLOCK_ID_QUESTIONS, "DEPTH_LEVEL" => 1, "ACTIVE" => "Y", "GLOBAL_ACTIVE" => "Y"],
                "order" => ["SORT" => "ASC"]
            )
        );
        $count = 0;
        while ($row = $res->fetch()) {
            $count++;
            $id = (int)$row['ID'];
            $sections[$id]['id'] = $count;
            $sections[$id]['sectionId'] = $id;
            $sections[$id]['theme'] = $row['NAME'];
        }
        return $sections;
    }

    private function getQuestions() {
        $sort = $this->getSort();
        $select = $this->ormManager()
            ->createSelectRequest(Question::class)
            ->equal('active', 'Y')
            ->addSort($sort['by'], $sort['order'])
            ->withRelations(['answers', 'expert']);

        return $select->getCollection();
    }

    private function getSort() {
        return array(
            'by' => 'sort',
            'order' => 'asc',
        );
    }

    private function getAnswers(Question $question) {
        $answers = [];
        $count = 0;
        foreach ($question->answers as $answer) {
            $count++;
            $item['id'] = $count;
            $item['answerId'] = $answer->id;
            $item['value'] = $answer->name;
            $answers[] = $item;
        }
        return $answers;
    }

    private function getApp() {
        global $APPLICATION;
        $this->app = $APPLICATION;
    }
}