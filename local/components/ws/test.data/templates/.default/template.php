<?php

use Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * @var array $arResult
 * @var array $arParams
 * @var CBitrixComponentTemplate $this
 * @var string $templateFolder
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @global CDataBase $DB
 */
$this->setFrameMode(true);

$fields = $arResult['fields'];
?>

<section id="top">
    <div class="container">
        <div id="test" class="test">
            <div class="status-bar">
                <div class="status-bar__item active done"></div>
                <div class="status-bar__item active done"></div>
                <div class="status-bar__item active current">
                    <div class="status-bar-question current done"></div>
                    <div class="status-bar-question current active"></div>
                    <div class="status-bar-question"></div>
                </div>
                <div class="status-bar__item"></div>
                <div class="status-bar__item"></div>
                <div class="status-bar__item"></div>
                <div class="status-bar__item"></div>
                <div class="status-bar__item"></div>
                <div class="status-bar__item"></div>
                <div class="status-bar__item"></div>
            </div>
            <div class="content">
                <div class="question-block">
                    <h2 class="title"></h2>
                    <span class="question-count"></span>
                    <h4 class="question"></h4>
                    <form action="">
                        <div class="question-options"></div>
                    </form>
                </div>

                <div class="expert-comment"></div>
            </div>

        </div>

    </div>

</section>
<div class="next">
    <a href="#" class="button with-icon active"><?=Loc::getMessage('CT_NEXT_BUTTON_TEXT');?></a>
</div>
<div class="loader"></div>