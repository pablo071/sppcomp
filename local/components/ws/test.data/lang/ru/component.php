<?php
$MESS['CT_COMP_FIELD_NAME'] = 'Меня зовут';
$MESS['CT_COMP_FIELD_NAME_PH'] = 'ваше имя';
$MESS['CT_COMP_FIELD_WORK_STATUS'] = 'я занимаю должность';
$MESS['CT_COMP_FIELD_WORK_STATUS_PH'] = 'ваша должность';
$MESS['CT_COMP_FIELD_COMPANY'] = 'в компании';
$MESS['CT_COMP_FIELD_COMPANY_PH'] = 'название компании';
$MESS['CT_COMP_FIELD_VIEW'] = 'Продуктовая разработка';
$MESS['CT_COMP_FIELD_VIEW_PH'] = 'моей основной деятельностью';
$MESS['CT_COMP_FIELD_VIEW_YES'] = 'является';
$MESS['CT_COMP_FIELD_VIEW_NO'] = 'не является';
